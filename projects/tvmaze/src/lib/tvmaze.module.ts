import { NgModule } from '@angular/core';
import { TvmazeComponent } from './tvmaze.component';
import { PosterComponent } from './poster/poster.component';

@NgModule({
  declarations: [TvmazeComponent, PosterComponent],
  imports: [
  ],
  exports: [TvmazeComponent, PosterComponent]
})
export class TvmazeModule { }
